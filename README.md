# addtask

Дополнительные задания




#Для запуска проекта скопируйте указанный ниже код
#Вставьте с ПОЛНОЙ заменой в файл tasks.json
#Расположение файла tasks.json: Explorer => первая папка ".theia" => tasks.json

# Запуск c пакетами ( пока что не готово, ниже заготовка )


{
    "tasks": [
        {
            "type": "che",
            "label": "*вставьте_название_нужного_файла* build and run",
            "command": "javac -d /projects/addtask/bin -sourcepath /projects/addtask/src/*вставьте_оставшийся_путь*/qsoft /projects/addtask/src/*вставьте_оставшийся_путь*/qsoft/*название_файла_с_расширением.java* && java -classpath /projects/addtask/bin *вставьте_название_пакета_и_запускаемого_файла_как_в_примере => ru.qdts.p2.Sherlock*",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/addtask/src",
                "component": "maven"
            }
        }
    ]
}
